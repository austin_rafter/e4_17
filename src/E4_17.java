import java.util.Scanner;
/*
* Austin Rafter
* 013433901
* Cs 049J
* 9/10/2020
*
* Take user input for two times as military time
*  print elapsed hours and minutes
 */


public class E4_17 {
    public static void main(String[] args) {
        Scanner myScanObjectOne = new Scanner(System.in);  //object for scanner to enter first time

        System.out.println("Please enter the first time: ");  //prompt for first time

        int nFirstTimeEntered = myScanObjectOne.nextInt(); //User enters first time

        Scanner myScanObjectTwo = new Scanner(System.in); //object for scanner to enter second time

        System.out.println("Please enter the second time: ");  //prompt for second time

        int nSecondTimeEntered = myScanObjectTwo.nextInt(); //User enters second time

        /*
        Initialize variables for hours entered, minutes entered, total hours, and total minutes
         */
        int nFirstTimeHours = 0;

        int nSecondTimeHours = 0;

        int nFirstTimeMinutes = 0;

        int nSecondTimeMinutes = 0;

        int nTotalHours = 0;

        int nTotalMinutes = 0;


        /*
        Check that first hours entered is between 0 and 24
         */
        if( (nFirstTimeEntered  <= 2400) && (nFirstTimeEntered > 0) )
        {   nFirstTimeHours = (nFirstTimeEntered / 100);  //get hours of the first entered time

        } else
            {   System.out.println("Sorry you cannot enter more than 24 for the hours of the first entered time");

                System.exit(0); //System exit
                // found at https://www.geeksforgeeks.org/system-exit-in-java/#:~:text=System.exit%28%29%20in%20Java.%20The%20java.lang.System.exit%28%29%20method%20exits%20current,abnormal%20termination.%20This%20is%20similar%20exit%20in%20C%2FC%2B%2B.
            }

        /*
        Check that first minutes entered is between 0 and 60
         */
        if( ( (nFirstTimeEntered % 100) < 60)  && ( (nFirstTimeEntered % 100) >= 0) )
        {   nFirstTimeMinutes = nFirstTimeEntered % 100;  //get minutes of the first entered time

        } else
            {   System.out.println("Sorry you cannot enter more than 59 for the minutes of the first entered time");
                System.exit(0);
            }

        /*
        Check that second hours entered is  between 0 and 24
         */
        if( (nSecondTimeEntered  <= 2400) && (nSecondTimeEntered > 0) )
        {   nSecondTimeHours = nSecondTimeEntered / 100;  //get hours of the second entered time

        } else
            {   System.out.println("Sorry you cannot enter more than 24 for the hours of the second entered time");
                System.exit(0);
            }


        /*
        Check that second minutes entered is between 0 and 60
         */
        if( ( (nSecondTimeEntered % 100) < 60) && ( (nSecondTimeEntered % 100) >= 0) )
        {   nSecondTimeMinutes = nSecondTimeEntered % 100;  //get minutes of the second entered time
        } else
            {   System.out.println("Sorry you cannot enter more than 59 for the minutes of the second entered time");
                System.exit(0);
            }


        /*
        Check if second time is greater than the first.
        If so then subtract the first hours from second hours and get total hours
        If not then add 24 to hours to time 2 and subtract time 1 from the new time 2
         */
        if(nSecondTimeEntered >= nFirstTimeEntered)
        {   nTotalHours =nSecondTimeHours - nFirstTimeHours; //set total hours to end time hours minus start time hours

            nTotalMinutes = nSecondTimeMinutes - nFirstTimeMinutes;  //set total minutes to subtraction of first and second minutes


            /*
            if total minutes is less than 0 add 60 to minutes and subtract an hour and print the output
            otherwise just print out the output
             */
            if( nTotalMinutes < 0)
                {   nTotalHours = nTotalHours - 1; //subtract 1 from total hours
                    nTotalMinutes = nTotalMinutes + 60; //add 60 to total minutes
                    System.out.println(nTotalHours + " Hours " + nTotalMinutes + " Minutes");
                } else
                    {   System.out.println(nTotalHours + " Hours " + nTotalMinutes + " Minutes");
                    }

        } else
                {   int nSecondTimePlusDay = nSecondTimeEntered + 2400;  //add a 24 hours to second entered time hours
                    nTotalHours = (nSecondTimePlusDay - nFirstTimeEntered) / 100; //subtract first time from fixed second time
                    // divide by 100 to get total hours elapsed


                    nTotalMinutes = nSecondTimeMinutes - nFirstTimeMinutes; //set total minutes to subtraction of first and second minutes
                    //to get total minutes elapsed



                    /*
                    Check if total minutes is less than 0 then add 60 to minutes and print output
                    otherwise just print out the output
                     */
                            if( nTotalMinutes < 0) {
                                nTotalMinutes = nTotalMinutes + 60; //add 60 to total minutes
                                System.out.println(nTotalHours + " Hours " + nTotalMinutes + " Minutes");
                            }
                            else{
                                System.out.println(nTotalHours + " Hours " + nTotalMinutes + " Minutes");
                                }

                }

    }

}
